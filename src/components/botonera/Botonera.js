import './Botonera.css';

import navState from "../../redux/reducers/navReducer";
import { nav_click } from "../../redux/actions/navActions";

import { connect } from 'react-redux';
 
import Home from "../home/Home";
import List from "../list/List";
import Form from "../form/Form";
import { useEffect } from 'react';

import { Link } from "react-router-dom";

const Botonera = ({title, nav_click, match: {params} }) => {

  console.log("params", params)

  const buttonClick = (p_title) => {
    console.log("buttonClick")
    nav_click({
      title: p_title 
    })
  }
  const renderSection = () => {
    switch(params.section){
      case 'home':
        return <Home/>
        break;
      case 'list':
        return <List/>
        break;
      case 'new':
        return <Form/>
        break;
      default:
        return;
    }
  }

  useEffect(()=>{ // cuando se monta el componente
    let p_title = ""
    switch(params.section){
      case 'home':
        p_title = "home"
        break;
      case 'list':
        p_title = "list"
        break;
      case 'new':
        p_title = "New"
        break;
      default:
        return 'home';
        break;
    }
    nav_click({
      title: p_title 
    })
  },[]) //como array para que se use solamente una vez

  return (
    <div className="Botonera">
      <header className="App-header">
        <div>
            <h1>{title}</h1>
        </div>
        <Link onClick={()=> buttonClick("Home")} to="Home">Home</Link>
        <Link onClick={()=> buttonClick("List")} to="List">List</Link>
        <Link onClick={()=> buttonClick("New")} to="News">News</Link>
      </header>
      {renderSection()}
    </div>
  );
}

function mapStateToProps(state, props) {
  // armamos un objeto solo con los
  // datos del store que nos interesan
  // y lo devolvemos  
  console.log("estado recibido", state)
  console.log("props: ",props)
  return {
    title: state.navState.title,
    section: props.section,
  };
}
//function mapDispatchToProps(dispatch, props) {
//  // creamos un objeto con un método para crear
//  // y despachar acciones fácilmente y en
//  // una sola línea
//  const actions = {
//    sendData: bindActionCreators(sendData, dispatch),
//  };
//  
//  // devolvemos nuestras funciones dispatch
//  // y los props normales del componente
//  return { actions };
//}

export default connect(
  mapStateToProps,
  { nav_click } // dispatch de solo uno
)(Botonera)