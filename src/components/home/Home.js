
import './Home.css';

import { connect } from 'react-redux';

function mapStateToProps(state, props) {
  // armamos un objeto solo con los
  // datos del store que nos interesan
  // y lo devolvemos  
  return {
  };
}
 
const Home = ({}) => {

  return (
    <div className="Home">
        <h1>Home</h1>
    </div>
  );
}

export default connect(
  mapStateToProps,
  {}
)(Home)