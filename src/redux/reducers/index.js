import { combineReducers } from "redux";

import navState from "./navReducer";

const todoApp = combineReducers({
    navState
})
  
export default todoApp