import * as types from '../actions/actionTypes';

const initialState = {
    title: "Home"
}

const navState = (previousState = initialState,action) => {
    // newState
    switch (action.type) {
        case types.NAV_CLICK:
          return {
            ...previousState,
            title: action.payload.title
          }
        default:
          return previousState
    }
}
export default navState;