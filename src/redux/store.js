import { createStore } from "redux";
import index from './reducers'
let store = createStore(
                    index, /* preloadedState, */
                    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
                    )
export default store