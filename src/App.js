import logo from './logo.svg';
import './App.css';

import { BrowserRouter, Switch, Route, Link } from "react-router-dom";

import Botonera from "./components/botonera/Botonera";
import Home from "./components/home/Home";
import List from "./components/list/List";
import Form from "./components/form/Form";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path="/:section" component={Botonera}>
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
